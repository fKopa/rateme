package br.com.rateme.model;

public class ResultadoChave {
	
	private Integer id;
	private Integer idObjetivos;
	private Integer idCategoria;   
    private String dataLimite;
    private String nome;
    private String descricao;
    private String dataCriacao;
    private String  erro;
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the idObjetivos
	 */
	public Integer getIdObjetivos() {
		return idObjetivos;
	}
	/**
	 * @param idObjetivos the idObjetivos to set
	 */
	public void setIdObjetivos(Integer idObjetivos) {
		this.idObjetivos = idObjetivos;
	}
	/**
	 * @return the idCategoria
	 */
	public Integer getIdCategoria() {
		return idCategoria;
	}
	/**
	 * @param idCategoria the idCategoria to set
	 */
	public void setIdCategoria(Integer idCategoria) {
		this.idCategoria = idCategoria;
	}
	/**
	 * @return the dataLimite
	 */
	public String getDataLimite() {
		return dataLimite;
	}
	/**
	 * @param dataLimite the dataLimite to set
	 */
	public void setDataLimite(String dataLimite) {
		this.dataLimite = dataLimite;
	}
	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	/**
	 * @return the dataCriacao
	 */
	public String getDataCriacao() {
		return dataCriacao;
	}
	/**
	 * @param dataCriacao the dataCriacao to set
	 */
	public void setDataCriacao(String dataCriacao) {
		this.dataCriacao = dataCriacao;
	}
	/**
	 * @return the erro
	 */
	public String getErro() {
		return erro;
	}
	/**
	 * @param erro the erro to set
	 */
	public void setErro(String erro) {
		this.erro = erro;
	}
    
}

package br.com.rateme.model;

public class Cargo {
	
	/** Id do cargo do Usuario */
	private int cargoId;
	
	/** Nome do cargo do Usuario */
	private String cargoNome;

	/**
	 * @return the cargoId
	 */
	public int getCargoId() {
		return cargoId;
	}

	/**
	 * @param cargoId the cargoId to set
	 */
	public void setCargoId(int cargoId) {
		this.cargoId = cargoId;
	}

	/**
	 * @return the cargoNome
	 */
	public String getCargoNome() {
		return cargoNome;
	}

	/**
	 * @param cargoNome the cargoNome to set
	 */
	public void setCargoNome(String cargoNome) {
		this.cargoNome = cargoNome;
	}
	

}

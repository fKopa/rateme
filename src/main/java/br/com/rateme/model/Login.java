package br.com.rateme.model;

public class Login {
		
	/** Flag para login valido */
	private boolean logado;
	
	/** Email para login */
	private String email;
	
	/** Senha para login */
	private String senha;
	
	/** Mensagem de erro login */
	private String erro;

	/**
	 * @return the login
	 */
	public boolean isLogado() {
		return logado;
	}

	/**
	 * @param login the login to set
	 */
	public void setLogado(boolean logado) {
		this.logado = logado;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the senha
	 */
	public String getSenha() {
		return senha;
	}

	/**
	 * @param senha the senha to set
	 */
	public void setSenha(String senha) {
		this.senha = senha;
	}

	/**
	 * @return the erro
	 */
	public String getErro() {
		return erro;
	}

	/**
	 * @param erro the erro to set
	 */
	public void setErro(String erro) {
		this.erro = erro;
	}
	
}

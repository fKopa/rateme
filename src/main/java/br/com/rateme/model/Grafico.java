package br.com.rateme.model;

public class Grafico {
	
        private String name;
        private int[] data;
		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}
		/**
		 * @param name the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}
		/**
		 * @return the data
		 */
		public int[] getData() {
			return data;
		}
		/**
		 * @param data the data to set
		 */
		public void setData(int[] data) {
			this.data = data;
		}
        
        
}

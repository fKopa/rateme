package br.com.rateme.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Objetivo {
	
	private Integer id;
    private String nome;
    private String descricao;
    private Double progress;
    private Integer idModerador;
    private Integer idGrupo;
    private Date datacriaco;
    private String dataLimite;
    private List<ResultadoChave> listResultadoChave = new ArrayList<ResultadoChave>();
    
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	/**
	 * @return the progress
	 */
	public double getProgress() {
		return progress;
	}
	/**
	 * @param progress the progress to set
	 */
	public void setProgress(double progress) {
		this.progress = progress;
	}
	/**
	 * @return the idModerador
	 */
	public Integer getIdModerador() {
		return idModerador;
	}
	/**
	 * @param idModerador the idModerador to set
	 */
	public void setIdModerador(Integer idModerador) {
		this.idModerador = idModerador;
	}
	/**
	 * @return the idGrupo
	 */
	public Integer getIdGrupo() {
		return idGrupo;
	}
	/**
	 * @param idGrupo the idGrupo to set
	 */
	public void setIdGrupo(Integer idGrupo) {
		this.idGrupo = idGrupo;
	}
	/**
	 * @return the datacriaco
	 */
	public Date getDatacriaco() {
		return datacriaco;
	}
	/**
	 * @param datacriaco the datacriaco to set
	 */
	public void setDatacriaco(Date datacriaco) {
		this.datacriaco = datacriaco;
	}
	/**
	 * @return the dataLimite
	 */
	public String getDataLimite() {
		return dataLimite;
	}
	/**
	 * @param dataLimite the dataLimite to set
	 */
	public void setDataLimite(String dataLimite) {
		this.dataLimite = dataLimite;
	}
	/**
	 * @return the listResultadoChave
	 */
	public List<ResultadoChave> getListResultadoChave() {
		return listResultadoChave;
	}
	/**
	 * @param listResultadoChave the listResultadoChave to set
	 */
	public void setListResultadoChave(List<ResultadoChave> listResultadoChave) {
		this.listResultadoChave = listResultadoChave;
	}	
    
}

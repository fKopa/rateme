package br.com.rateme.model;

public class Comentario {
	private Integer id;
	private Integer idUsuarioAvlr;
	private Integer idUsuarioAvl;
	private Integer idCategoria;
	private String nomeCategoria;
	private String descricao;
    private String nota;
    private boolean favoritado;
    private String erro;
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the idUsuarioAvlr
	 */
	public Integer getIdUsuarioAvlr() {
		return idUsuarioAvlr;
	}
	/**
	 * @param idUsuarioAvlr the idUsuarioAvlr to set
	 */
	public void setIdUsuarioAvlr(Integer idUsuarioAvlr) {
		this.idUsuarioAvlr = idUsuarioAvlr;
	}
	/**
	 * @return the idUsuarioAvl
	 */
	public Integer getIdUsuarioAvl() {
		return idUsuarioAvl;
	}
	/**
	 * @param idUsuarioAvl the idUsuarioAvl to set
	 */
	public void setIdUsuarioAvl(Integer idUsuarioAvl) {
		this.idUsuarioAvl = idUsuarioAvl;
	}
	/**
	 * @return the idCategoria
	 */
	public Integer getIdCategoria() {
		return idCategoria;
	}
	/**
	 * @param idCategoria the idCategoria to set
	 */
	public void setIdCategoria(Integer idCategoria) {
		this.idCategoria = idCategoria;
	}
	
	/**
	 * @return the nomeCategoria
	 */
	public String getNomeCategoria() {
		return nomeCategoria;
	}
	/**
	 * @param nomeCategoria the nomeCategoria to set
	 */
	public void setNomeCategoria(String nomeCategoria) {
		this.nomeCategoria = nomeCategoria;
	}
	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	/**
	 * @return the nota
	 */
	public String getNota() {
		return nota;
	}
	/**
	 * @param nota the nota to set
	 */
	public void setNota(String nota) {
		this.nota = nota;
	}
	/**
	 * @return the favoritado
	 */
	public boolean isFavoritado() {
		return favoritado;
	}
	/**
	 * @param favoritado the favoritado to set
	 */
	public void setFavoritado(boolean favoritado) {
		this.favoritado = favoritado;
	}
	/**
	 * @return the erro
	 */
	public String getErro() {
		return erro;
	}
	/**
	 * @param erro the erro to set
	 */
	public void setErro(String erro) {
		this.erro = erro;
	}
    
}

package br.com.rateme.model;

public class Cadastro {
	
	/** Nome do Usuario */
	private String nome;
	
	/** Idade do Usuario */
	private int idade;
	
	/** Cargo do Usuario */
	private Cargo cargo;
	
	/** Descricao do Usuario sobre o seu cargo */
	private String descUser;
	
	/** Email do Usuario */
	private String email;
	
	/** Senha do Usuario */
	private String senha;
	
	/** Mensagem de erro cadastro */
	private String erro;
	
	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * @return the idade
	 */
	public int getIdade() {
		return idade;
	}
	/**
	 * @param idade the idade to set
	 */
	public void setIdade(int idade) {
		this.idade = idade;
	}
	/**
	 * @return the cargoId
	 */
	public Cargo getCargo() {
		return cargo;
	}
	/**
	 * @param cargoId the cargoId to set
	 */
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	/**
	 * @return the descUser
	 */
	public String getDescUser() {
		return descUser;
	}
	/**
	 * @param descUser the descUser to set
	 */
	public void setDescUser(String descUser) {
		this.descUser = descUser;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the senha
	 */
	public String getSenha() {
		return senha;
	}
	/**
	 * @param senha the senha to set
	 */
	public void setSenha(String senha) {
		this.senha = senha;
	}
	/**
	 * @return the erro
	 */
	public String getErro() {
		return erro;
	}
	/**
	 * @param erro the erro to set
	 */
	public void setErro(String erro) {
		this.erro = erro;
	}
	
}

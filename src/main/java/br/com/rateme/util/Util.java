package br.com.rateme.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Util {
	
	/**
     * Convert JSON string to object.
     *
     * @param <T>   the generic type
     * @param json  the json
     * @param clazz the clazz
     * @return the t
     * @throws InfraException the infra exception
     */
	public static <T> T convertJSONStringToObject(String json, Class<T> clazz) throws Exception{
		System.out.println("Retorno: "+json);
        try {
            if (json == null) {
                return null;
            }
            Gson gson = new GsonBuilder().serializeNulls().create();
            return gson.fromJson(json, clazz);
        } catch (Exception e) {
            throw new Exception("Problema ao gerar o Json");
        }
    } 
	
	/**
     * Converte objeto para string json.
     * @param text the text
     * @return the string
     */
    public static String convertObjectToJsonString(Object objeto) {
        Gson gson = new Gson();
        if (objeto == null) {
            return null;
        }
        System.out.println("Parametro: "+gson.toJson(objeto));
        return gson.toJson(objeto);
    }
    
    /**
     * Cria chamada http post com parametros
     * @param String url - Url so serviço
     * @param StringEntity params - Json
     * @return the Json string
     * @throws Exception 
     */
    public static String criarChamadaHttpPost(String url, StringEntity params) throws Exception {
    	HttpClient httpClient = HttpClientBuilder.create().build();
    	StringBuilder sb = new StringBuilder();
		try {
			
		    HttpPost request = new HttpPost(url);
		    request.addHeader("content-type", "application/json");
		    request.setEntity(params);
		    HttpResponse response = httpClient.execute(request);
		    HttpEntity entity = response.getEntity();
		    InputStream instream = entity.getContent();
		    
		    BufferedReader reader = new BufferedReader(new InputStreamReader(instream));
		    sb.append(reader.readLine());
		    
		}catch (Exception ex) {
			throw new Exception("Sistema Indisponivel, tente novamente mais tarde.");
		}
		return sb.toString();
    }
    
    
    /**
     * Cria chamada http post sem parametros
     * @param String url - Url so serviço
     * @param StringEntity params - Json
     * @return the Json string
     * @throws Exception 
     */
    public static String criarChamadaHttpPost(String url) throws Exception {
    	HttpClient httpClient = HttpClientBuilder.create().build();
    	StringBuilder sb = new StringBuilder();
		try {

		    HttpPost request = new HttpPost(url);
		    request.addHeader("content-type", "application/json");
		    HttpResponse response = httpClient.execute(request);
		    HttpEntity entity = response.getEntity();
		    InputStream instream = entity.getContent();
		    
		    BufferedReader reader = new BufferedReader(new InputStreamReader(instream));
		    sb.append(reader.readLine());
		    
		}catch (Exception ex) {
			throw new Exception("Sistema Indisponivel, tente novamente mais tarde.");
		}
		return sb.toString();
    }
    
    /**
     * Cria chamada http get com parametros
     * @param String url - Url so serviço
     * @param StringEntity param
     * @return the Json string
     * @throws Exception 
     */
    public static String criarChamadaHttpGet(String url, String param) throws Exception {
    	HttpClient httpClient = HttpClientBuilder.create().build();
    	StringBuilder sb = new StringBuilder();
		try {
			HttpGet request = new HttpGet(url+param);
			HttpResponse response = httpClient.execute(request);
		    HttpEntity entity = response.getEntity();
		    InputStream instream = entity.getContent();
		    
		    BufferedReader reader = new BufferedReader(new InputStreamReader(instream));
		    sb.append(reader.readLine());
		    
		}catch (Exception ex) {
			throw new Exception("Sistema Indisponivel, tente novamente mais tarde.");
		}
		return sb.toString();
    }
    
    /**
     * Recupera uma propriedade do arquivo rateme.properties
     * @param String propName - nome da propriedade
     * @return String contendo o valor da propriedade
     * @throws IOException
     */
    public static String getProp(String propName) throws IOException {
        Properties prop = new Properties();
        String separator = System.getProperty("file.separator");
        StringBuilder builder = new StringBuilder();
        builder.append(separator);
        builder.append("rateme");
        builder.append(separator);
        builder.append("rateme.properties");
        FileInputStream file = new FileInputStream(builder.toString());
        prop.load(file);
        return prop.getProperty(propName);
    }
    
    /**
     * Valida se existe uma sessão valida
     * @param HttpSession session - sessão
     * @return boolean contendo o valor flag de validação de sessão
     */
    public static boolean validaSessao(HttpSession session) {
    	boolean retorno = true;
    	if(session.getAttribute("usuario") == null) {
    		retorno = false;
    	}
    	return retorno;
    }

}

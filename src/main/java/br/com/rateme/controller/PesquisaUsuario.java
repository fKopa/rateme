package br.com.rateme.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.rateme.model.Comentario;
import br.com.rateme.model.Grafico;
import br.com.rateme.model.Objetivo;
import br.com.rateme.model.Usuario;
import br.com.rateme.util.Util;

@Controller
public class PesquisaUsuario {
	
	private static final String URL_PESQUISA_USUARIOS = "url.search.user";
	
	@RequestMapping("pesquisausuarios")
	public ModelAndView pesquisaUsuarios(HttpSession session) {
		ModelAndView modelAndView = null;
		if(Util.validaSessao(session)) {
			Usuario usuario = (Usuario)session.getAttribute("usuario");
			modelAndView = new ModelAndView("pesquisarusuarios");
			modelAndView.addObject("usuario", usuario);
		}else {
			modelAndView = new ModelAndView("login");
		}
		return modelAndView;
	}
	
	@RequestMapping("pesquisarusuario")
	public ModelAndView pesquisarUsuario(String pesquisa, HttpSession session) {
		ModelAndView modelAndView = null;
		if(Util.validaSessao(session)) {
			List<Usuario> usuarios = new ArrayList<Usuario>();
			usuarios = listarUsuarios(pesquisa);
			Usuario usuario = (Usuario)session.getAttribute("usuario");
			modelAndView = new ModelAndView("pesquisarusuarios");
			modelAndView.addObject("usuario", usuario);
			modelAndView.addObject("listUsuarios", usuarios);
			modelAndView.addObject("listUsuariosJson", Util.convertObjectToJsonString(usuarios));
		}else {
			modelAndView = new ModelAndView("login");
		}
		return modelAndView;
	}
	
	@RequestMapping("exibirperfilusuario")
	public ModelAndView exibirPerfilSel(String idUsuarioSel, String listUsuariosJson, HttpSession session) {
		ModelAndView modelAndView = null;
		if(Util.validaSessao(session)) {
			try {
				Usuario usuario = buscarUsuario(idUsuarioSel, listUsuariosJson);
				String json = "[{name: 'Java', data: [0, 4, 8]}, {name: 'MySql ', data: [0, 5, 7]}]";
				Grafico[] grafico = Util.convertJSONStringToObject(json, Grafico[].class);
				json = Util.convertObjectToJsonString(grafico);
				String usuarioJson = Util.convertObjectToJsonString(usuario);
				List<Objetivo> listObjetivos = new ArrayList<Objetivo>();
				ObjetivoUsuario objetivoUsuario = new ObjetivoUsuario();
				listObjetivos = objetivoUsuario.listarObjetivos(usuario.getId());
				List<Comentario> listComentarios = new ArrayList<Comentario>();
				ComentarioUsuario comentarioUsuario = new ComentarioUsuario();
				listComentarios = comentarioUsuario.listarComentariosAvaliado(usuario);
				
				modelAndView = new ModelAndView("perfilusuario");
				modelAndView.addObject("listComentarios", listComentarios);
				modelAndView.addObject("chartLine" ,json);
				modelAndView.addObject("usuario", usuario);
				modelAndView.addObject("usuarioJson", usuarioJson);
				modelAndView.addObject("isMyUser" ,false);
				modelAndView.addObject("listObjetivos", listObjetivos);
				modelAndView.addObject("listObjetivosJson", Util.convertObjectToJsonString(listObjetivos));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			modelAndView = new ModelAndView("login");
		}
		return modelAndView;
	}
	
	public List<Usuario> listarUsuarios(String pesquisa){
		List<Usuario> listUsuarios = new ArrayList<Usuario>();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("?nome=");
		stringBuilder.append(pesquisa);
		try {
			Usuario[] usuarios;
			String responseJSon = Util.criarChamadaHttpGet(Util.getProp(URL_PESQUISA_USUARIOS), stringBuilder.toString());
			usuarios = Util.convertJSONStringToObject(responseJSon, Usuario[].class);
			listUsuarios = Arrays.asList(usuarios);
		}catch (Exception e) {
		}
		return listUsuarios;
	}
	
	public Usuario buscarUsuario(String idUsuarioSel, String listUsuariosJson) {
		Usuario usuarioSel = null;
		try {
			Usuario[] Usuarios;
			Usuarios = Util.convertJSONStringToObject(listUsuariosJson, Usuario[].class);
			List<Usuario> listUsuarios = new ArrayList<Usuario>();
			listUsuarios = Arrays.asList(Usuarios);
			for(Usuario usuario : listUsuarios) {
				if(usuario.getId().toString().equals(idUsuarioSel)) {
					usuarioSel = usuario;
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return usuarioSel;
	}
}

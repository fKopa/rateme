package br.com.rateme.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.http.entity.StringEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.rateme.model.Categoria;
import br.com.rateme.model.Comentario;
import br.com.rateme.model.Grafico;
import br.com.rateme.model.Objetivo;
import br.com.rateme.model.Usuario;
import br.com.rateme.util.Util;

@Controller
public class ComentarioUsuario {
	
	private static final String URL_CADASTRO_COMENTARIO = "url.create.comentario";
	private static final String URL_LISTA_CATEGORIAS = "url.list.categoria";
	private static final String URL_LISTA_COMENTARIO_AVALIADO = "url.list.comentario.avaliado";
	
	@RequestMapping("cadastrocomentario")
	public ModelAndView cadastroComentario(String usuarioJson, HttpSession session) {
		ModelAndView modelAndView = null;
		if(Util.validaSessao(session)) {
			try { 
				List<Categoria> listCategorias = listarCategorias();
				Usuario usuario = Util.convertJSONStringToObject(usuarioJson, Usuario.class);
				modelAndView = new ModelAndView("comentarios/cadastrocomentario");
				modelAndView.addObject("isMyUser" ,false);
				modelAndView.addObject("usuario", usuario);
				modelAndView.addObject("usuarioJson", usuarioJson);
				modelAndView.addObject("listCategorias", listCategorias);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			modelAndView = new ModelAndView("login");
		}
		return modelAndView;
	}
	
	@RequestMapping("comentarioperfil")
	public ModelAndView comentarioPerfil(String usuarioJson, HttpSession session) {
		ModelAndView modelAndView = null;
		if(Util.validaSessao(session)) {
			try {
				Usuario usuario = Util.convertJSONStringToObject(usuarioJson, Usuario.class);
				String json = "[{name: 'Java', data: [0, 4, 8]}, {name: 'MySql ', data: [0, 5, 7]}]";
				Grafico[] grafico = Util.convertJSONStringToObject(json, Grafico[].class);
				json = Util.convertObjectToJsonString(grafico);
				List<Objetivo> listObjetivos = new ArrayList<Objetivo>();
				ObjetivoUsuario objetivoUsuario = new ObjetivoUsuario();
				listObjetivos = objetivoUsuario.listarObjetivos(usuario.getId());
				List<Comentario> listComentarios = new ArrayList<Comentario>();
				ComentarioUsuario comentarioUsuario = new ComentarioUsuario();
				listComentarios = comentarioUsuario.listarComentariosAvaliado(usuario);
				
				modelAndView = new ModelAndView("perfilusuario");
				modelAndView.addObject("listComentarios", listComentarios);
				modelAndView.addObject("chartLine" ,json);
				modelAndView.addObject("usuario", usuario);
				modelAndView.addObject("usuarioJson", usuarioJson);
				modelAndView.addObject("isMyUser" ,false);
				modelAndView.addObject("listObjetivos", listObjetivos);
				modelAndView.addObject("listObjetivosJson", Util.convertObjectToJsonString(listObjetivos));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			modelAndView = new ModelAndView("login");
		}
		return modelAndView;
	}
	
	@RequestMapping("cadastrarcomentario")
	public ModelAndView cadastrarComentario(Comentario comentario, String usuarioJson, HttpSession session) {
		ModelAndView modelAndView = null;
		if(Util.validaSessao(session)) {
			Usuario usuario = (Usuario)session.getAttribute("usuario");
			comentario.setIdUsuarioAvlr(usuario.getId());
			Usuario usuarioAvaliado =  new Usuario();
			try {
				usuarioAvaliado = Util.convertJSONStringToObject(usuarioJson, Usuario.class);
				comentario.setIdUsuarioAvl(usuarioAvaliado.getId());
				StringEntity params = new StringEntity(Util.convertObjectToJsonString(comentario));		    
				String responseJSon = Util.criarChamadaHttpPost(Util.getProp(URL_CADASTRO_COMENTARIO), params);
				modelAndView = new ModelAndView("comentarios/comentariousuario");
				modelAndView.addObject("isMyUser" ,false);
				modelAndView.addObject("usuario" ,usuarioAvaliado);
				modelAndView.addObject("usuarioJson" ,usuarioJson);
				modelAndView.addObject("objetivo", comentario);
			}catch (Exception e) {
				try {
					modelAndView = new ModelAndView("perfilusuario");
					List<Comentario> listComentarios = new ArrayList<Comentario>();
					ComentarioUsuario comentarioUsuario = new ComentarioUsuario();
					listComentarios = comentarioUsuario.listarComentariosAvaliado(usuarioAvaliado);
					modelAndView.addObject("isMyUser" ,false);
					modelAndView.addObject("listComentarios", listComentarios);
					modelAndView.addObject("usuario" ,usuarioAvaliado);
					modelAndView.addObject("usuarioJson" ,usuarioJson);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}else {
			modelAndView = new ModelAndView("login");
		}
		return modelAndView;
	}
	
	@RequestMapping("comentariosusuario")
	public ModelAndView logar(HttpSession session) {
		ModelAndView modelAndView = null;
		try {
			Usuario usuario = (Usuario)session.getAttribute("usuario");
			if(usuario != null) {
					modelAndView = new ModelAndView("comentarios/comentariosusuario");
					
					List<Comentario> listComentarios = new ArrayList<Comentario>();
					ComentarioUsuario comentarioUsuario = new ComentarioUsuario();
					listComentarios = comentarioUsuario.listarComentariosAvaliado(usuario);
					
					modelAndView.addObject("usuario", usuario);
					modelAndView.addObject("listComentarios", listComentarios);
					modelAndView.addObject("listComentariosJson", Util.convertObjectToJsonString(listComentarios));
				
			}else {
				modelAndView = new ModelAndView("login");
				modelAndView.addObject("erro", true);
				modelAndView.addObject("msgErro", "Sistema indisponivel");
			}
		}catch (Exception e) {
			modelAndView = new ModelAndView("login");
			modelAndView.addObject("erro", true);
			modelAndView.addObject("msgErro", "Sistema indisponivel");
		}
		
		return modelAndView;
	}
	
	public List<Categoria> listarCategorias(){
		List<Categoria> listCategorias = new ArrayList<Categoria>();
		try {
			Categoria[] categorias;
			String responseJSon = Util.criarChamadaHttpPost(Util.getProp(URL_LISTA_CATEGORIAS));
			categorias = Util.convertJSONStringToObject(responseJSon, Categoria[].class);
			listCategorias = Arrays.asList(categorias);
		}catch (Exception e) {
			// TODO: handle exception
		}
		return listCategorias;
	}
	
	public List<Comentario> listarComentariosAvaliado(Usuario usuario) throws Exception{
		Comentario[] comentarios;
		List<Comentario> listComentarios = new ArrayList<Comentario>();
		Usuario usuarioRq = new Usuario();
		usuarioRq.setId(usuario.getId());
		List<Usuario> usuarios = new ArrayList<Usuario>();
		usuarios.add(usuarioRq);
		try {
			StringEntity params = new StringEntity(Util.convertObjectToJsonString(usuarios));
			String responseJSon = Util.criarChamadaHttpPost(Util.getProp(URL_LISTA_COMENTARIO_AVALIADO), params);
			comentarios = Util.convertJSONStringToObject(responseJSon, Comentario[].class);
			listComentarios = Arrays.asList(comentarios);
		}catch (Exception e) {
			throw new Exception();
		}
		return listComentarios;
	}
}

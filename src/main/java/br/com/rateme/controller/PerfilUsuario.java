package br.com.rateme.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.rateme.model.Comentario;
import br.com.rateme.model.Grafico;
import br.com.rateme.model.Objetivo;
import br.com.rateme.model.Usuario;
import br.com.rateme.util.Util;

@Controller
public class PerfilUsuario {
	@RequestMapping("exibirperfilusuariologado")
	public ModelAndView perfil(HttpSession session) {
		ModelAndView modelAndView = null;
		if(Util.validaSessao(session)) {
			Usuario usuario = (Usuario) session.getAttribute("usuario");
			modelAndView = new ModelAndView("perfilusuario");
			try {
				String json = "[{name: 'Java', data: [0, 4, 8]}, {name: 'MySql ', data: [0, 5, 7]}]";
				Grafico[] grafico = Util.convertJSONStringToObject(json, Grafico[].class);
				json = Util.convertObjectToJsonString(grafico);
				List<Objetivo> listObjetivos = new ArrayList<Objetivo>();
				ObjetivoUsuario objetivoUsuario = new ObjetivoUsuario();
				listObjetivos = objetivoUsuario.listarObjetivos(usuario.getId());
				List<Comentario> listComentarios = new ArrayList<Comentario>();
				ComentarioUsuario comentarioUsuario = new ComentarioUsuario();
				listComentarios = comentarioUsuario.listarComentariosAvaliado(usuario);
				
				modelAndView.addObject("listComentarios", listComentarios);
				modelAndView.addObject("chartLine" ,json);
				modelAndView.addObject("usuario" ,usuario);
				modelAndView.addObject("isMyUser" ,true);
				modelAndView.addObject("listObjetivos", listObjetivos);
				modelAndView.addObject("listObjetivosJson", Util.convertObjectToJsonString(listObjetivos));
			} catch (Exception e) {
			}
		}else {
			modelAndView = new ModelAndView("login");
		}
		return modelAndView;
	}
}

package br.com.rateme.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.entity.StringEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.rateme.model.Cadastro;
import br.com.rateme.model.Cargo;
import br.com.rateme.util.Util;

@Controller
public class CadastroUsuario {
	
	private static final String URL_CADASTRO_USUARIO = "url.create.user";
	private static final String URL_LISTA_CARGOS = "url.search.cargo";
	
	@RequestMapping("cadastro")
	public ModelAndView cadastro() {
		ModelAndView modelAndView = null;
		try {
			List<Cargo> listCargos = listarCargos();
			modelAndView = new ModelAndView("cadastro/cadastro");
			modelAndView.addObject("listCargos", listCargos);
			modelAndView.addObject("erro", false);
		} catch (Exception e) {
			modelAndView = new ModelAndView("cadastro/cadastro");
			modelAndView.addObject("erro", true);
			modelAndView.addObject("msgErro", e.getMessage());
		}
		return modelAndView;
	}
	
	@RequestMapping("cadastrarUsuario")
	public ModelAndView cadastrar(Cadastro cadastro, Model model) {
		ModelAndView modelAndView = null;
		try {
			StringEntity params = new StringEntity(Util.convertObjectToJsonString(cadastro));		    
			String responseJSon = Util.criarChamadaHttpPost(Util.getProp(URL_CADASTRO_USUARIO), params);
			Cadastro cadastroRs = Util.convertJSONStringToObject(responseJSon, Cadastro.class);
			
			if(cadastroRs.getErro() == null) {
				modelAndView = new ModelAndView("cadastro/sucessocadastro");
				modelAndView.addObject("cadastroRs", cadastroRs);
			}else {
				List<Cargo> listCargos = listarCargos();
				modelAndView = new ModelAndView("cadastro/cadastro");
				modelAndView.addObject("listCargos", listCargos);
				modelAndView.addObject("erro", true);
				modelAndView.addObject("msgErro", cadastroRs.getErro());
			}
		}catch (Exception e) {
			List<Cargo> listCargos = listarCargos();
			modelAndView = new ModelAndView("cadastro/cadastro");
			modelAndView.addObject("listCargos", listCargos);
			modelAndView.addObject("erro", true);
			modelAndView.addObject("msgErro", e.getMessage());
		}
		
		return modelAndView;
	}
	
	public List<Cargo> listarCargos(){
		List<Cargo> listCargos = new ArrayList<Cargo>();
		try {
			Cargo[] cargos;
			String responseJSon = Util.criarChamadaHttpPost(Util.getProp(URL_LISTA_CARGOS));
			cargos = Util.convertJSONStringToObject(responseJSon, Cargo[].class);
			listCargos = Arrays.asList(cargos);
		}catch (Exception e) {
			// TODO: handle exception
		}
		return listCargos;
	}
	
}

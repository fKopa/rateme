package br.com.rateme.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.rateme.model.Comentario;
import br.com.rateme.model.Objetivo;
import br.com.rateme.model.Usuario;
import br.com.rateme.util.Util;

@Controller
public class PaginaInicial {
	
	@RequestMapping("paginainicial")
	public ModelAndView paginaInicial(HttpSession session) {
		ModelAndView modelAndView = null;
		if(Util.validaSessao(session)) {
			try{
				modelAndView = new ModelAndView("paginainicial");
				Usuario usuario = (Usuario)session.getAttribute("usuario");
				List<Objetivo> listObjetivos = new ArrayList<Objetivo>();
				ObjetivoUsuario objetivoUsuario = new ObjetivoUsuario();
				listObjetivos = objetivoUsuario.listarObjetivos(usuario.getId());
				
				List<Comentario> listComentarios = new ArrayList<Comentario>();
				ComentarioUsuario comentarioUsuario = new ComentarioUsuario();
				listComentarios = comentarioUsuario.listarComentariosAvaliado(usuario);
				
				modelAndView.addObject("listComentarios", listComentarios);
				modelAndView.addObject("listObjetivos", listObjetivos);
				modelAndView.addObject("listObjetivosJson", Util.convertObjectToJsonString(listObjetivos));
			}catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			modelAndView = new ModelAndView("login");
		}
		return modelAndView;
	}
}

package br.com.rateme.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.http.entity.StringEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.rateme.model.Comentario;
import br.com.rateme.model.Login;
import br.com.rateme.model.Objetivo;
import br.com.rateme.model.Usuario;
import br.com.rateme.util.Util;

@Controller
public class LoginUsuario {
	
	private static final String URL_LOGIN = "url.login";
	
	@RequestMapping("login")
	public ModelAndView login() {

		ModelAndView modelAndView = new ModelAndView("login");
		return modelAndView;
	}
	
	@RequestMapping("logar")
	public ModelAndView logar(Login login, HttpSession session) {
		ModelAndView modelAndView = null;
		try {
			StringEntity params = new StringEntity(Util.convertObjectToJsonString(login));		    
			String ResponseJSon = Util.criarChamadaHttpPost(Util.getProp(URL_LOGIN), params);
			Usuario usuarioRs = Util.convertJSONStringToObject(ResponseJSon, Usuario.class);
			if(usuarioRs != null) {
				if(usuarioRs.getErro() == null) {
					session.setAttribute("usuario", usuarioRs);
					modelAndView = new ModelAndView("paginainicial");
					modelAndView.addObject("usuario", usuarioRs);
					Usuario usuario = (Usuario)session.getAttribute("usuario");
					List<Objetivo> listObjetivos = new ArrayList<Objetivo>();
					ObjetivoUsuario objetivoUsuario = new ObjetivoUsuario();
					listObjetivos = objetivoUsuario.listarObjetivos(usuario.getId());
					
					List<Comentario> listComentarios = new ArrayList<Comentario>();
					ComentarioUsuario comentarioUsuario = new ComentarioUsuario();
					listComentarios = comentarioUsuario.listarComentariosAvaliado(usuario);
					
					modelAndView.addObject("listComentarios", listComentarios);
					modelAndView.addObject("listObjetivos", listObjetivos);
					modelAndView.addObject("listObjetivosJson", Util.convertObjectToJsonString(listObjetivos));
				}else {
					modelAndView = new ModelAndView("login");
					modelAndView.addObject("erro", true);
					modelAndView.addObject("msgErro", usuarioRs.getErro());
				}
			}else {
				modelAndView = new ModelAndView("login");
				modelAndView.addObject("erro", true);
				modelAndView.addObject("msgErro", "Sistema indisponivel");
			}
		}catch (Exception e) {
			modelAndView = new ModelAndView("login");
			modelAndView.addObject("erro", true);
			modelAndView.addObject("msgErro", "Sistema indisponivel");
		}
		
		return modelAndView;
	}
}

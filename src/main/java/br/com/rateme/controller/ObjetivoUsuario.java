package br.com.rateme.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.http.entity.StringEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.rateme.model.Categoria;
import br.com.rateme.model.Comentario;
import br.com.rateme.model.Objetivo;
import br.com.rateme.model.ResultadoChave;
import br.com.rateme.model.Usuario;
import br.com.rateme.util.Util;

@Controller
public class ObjetivoUsuario {
	
	private static final String URL_CADASTRO_OBJETIVO = "url.create.objetivo";
	
	private static final String URL_LISTAGEM_OBJETIVO = "url.list.objetivo";
	
	private static final String URL_CADASTRO_RESULTADO = "url.create.resultado";
	
	private static final String URL_LISTAGEM_RESULTADOS = "url.list.resultado";
	
	private static final String URL_LISTA_CATEGORIAS = "url.list.categoria";
	
	@RequestMapping("cadastroobjetivo")
	public ModelAndView cadastroObjetivo(HttpSession session) {
		ModelAndView modelAndView = null;
		if(Util.validaSessao(session)) {
			modelAndView = new ModelAndView("objetivos/cadastroobjetivo");
		}else {
			modelAndView = new ModelAndView("login");
		}
		return modelAndView;
	}
	
	@RequestMapping("cadastrarobjetivo")
	public ModelAndView cadastrarObjetivo(Objetivo objetivo, HttpSession session) {
		ModelAndView modelAndView = null;
		if(Util.validaSessao(session)) {
			Usuario usuario = (Usuario)session.getAttribute("usuario");
			try {
				objetivo.setIdModerador(usuario.getId());
				StringEntity params = new StringEntity(Util.convertObjectToJsonString(objetivo));		    
				String responseJSon = Util.criarChamadaHttpPost(Util.getProp(URL_CADASTRO_OBJETIVO), params);
				modelAndView = new ModelAndView("objetivos/objetivousuario");
				modelAndView.addObject("usuario" ,usuario);
				modelAndView.addObject("objetivo", objetivo);
			}catch (Exception e) {
				try {
					modelAndView = new ModelAndView("perfilusuario");
					List<Comentario> listComentarios = new ArrayList<Comentario>();
					ComentarioUsuario comentarioUsuario = new ComentarioUsuario();
					listComentarios = comentarioUsuario.listarComentariosAvaliado(usuario);
					modelAndView.addObject("isMyUser" ,false);
					modelAndView.addObject("listComentarios", listComentarios);
					modelAndView.addObject("usuario" , usuario);
				}catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}else {
			modelAndView = new ModelAndView("login");
		}
		return modelAndView;
	}
	
	@RequestMapping("listarobjetivos")
	public ModelAndView listarObjetivo(HttpSession session) {
		ModelAndView modelAndView = null;
		if(Util.validaSessao(session)) {
			Usuario usuario = (Usuario)session.getAttribute("usuario");
			try {
				List<Objetivo> listObjetivos = new ArrayList<Objetivo>();
				listObjetivos = listarObjetivos(usuario.getId());
				modelAndView = new ModelAndView("objetivos/listagemobjetivos");
				modelAndView.addObject("listObjetivos", listObjetivos);
				modelAndView.addObject("listObjetivosJson", Util.convertObjectToJsonString(listObjetivos));
			}catch (Exception e) {
				try {
					modelAndView = new ModelAndView("perfilusuario");
					List<Comentario> listComentarios = new ArrayList<Comentario>();
					ComentarioUsuario comentarioUsuario = new ComentarioUsuario();
					listComentarios = comentarioUsuario.listarComentariosAvaliado(usuario);
					modelAndView.addObject("isMyUser" ,false);
					modelAndView.addObject("listComentarios", listComentarios);
					modelAndView.addObject("usuario" ,usuario);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}else {
			modelAndView = new ModelAndView("login");
		}
		/*modelAndView = new ModelAndView("objetivos/listagemobjetivos");
		modelAndView.addObject("usuario", usuario);*/
		return modelAndView;
	}
	
	@RequestMapping("exibirobjetivo")
	public ModelAndView exibirObjetivo(String idObjetivo, String listObjetivosJson, HttpSession session) {
		ModelAndView modelAndView = null;
		if(Util.validaSessao(session)) {
			try {
				Objetivo objetivo = buscarObjetivo(idObjetivo, listObjetivosJson);
				objetivo.setListResultadoChave(listarResultados(objetivo.getId()));
				modelAndView = new ModelAndView("objetivos/objetivousuario");
				modelAndView.addObject("objetivo", objetivo);
				modelAndView.addObject("objetivoJson", Util.convertObjectToJsonString(objetivo));
				modelAndView.addObject("usuario", (Usuario)session.getAttribute("usuario"));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			modelAndView = new ModelAndView("login");
		}
		return modelAndView;
	}
	
	@RequestMapping("cadastroresultado")
	public ModelAndView cadastroResultado(String objetivoJson, HttpSession session) {
		ModelAndView modelAndView = null;
		if(Util.validaSessao(session)) {
			try {
				
				List<Categoria> listCategorias = listarCategorias();
				modelAndView = new ModelAndView("objetivos/cadastroresultado");
				modelAndView.addObject("objetivoJson", objetivoJson);
				modelAndView.addObject("usuario", (Usuario)session.getAttribute("usuario"));
				modelAndView.addObject("listCategorias", listCategorias);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			modelAndView = new ModelAndView("login");
		}
		return modelAndView;
	}
	
	@RequestMapping("cadastrarresultado")
	public ModelAndView cadastrarResultado(ResultadoChave resultadoChave ,String objetivoJson, HttpSession session) {
		ModelAndView modelAndView = null;
		if(Util.validaSessao(session)) {
			Usuario usuario = (Usuario)session.getAttribute("usuario");
			try {
				Objetivo objetivo = Util.convertJSONStringToObject(objetivoJson, Objetivo.class);
				resultadoChave.setIdObjetivos(objetivo.getId());
				StringEntity params = new StringEntity(Util.convertObjectToJsonString(resultadoChave));		    
				String responseJSon = Util.criarChamadaHttpPost(Util.getProp(URL_CADASTRO_RESULTADO), params);
				modelAndView = new ModelAndView("objetivos/objetivousuario");
				objetivo.setListResultadoChave(listarResultados(objetivo.getId()));
				modelAndView.addObject("usuario" ,usuario);
				modelAndView.addObject("objetivo", objetivo);
			}catch (Exception e) {
				try {
					modelAndView = new ModelAndView("objetivos/objetivousuario");
					List<Comentario> listComentarios = new ArrayList<Comentario>();
					ComentarioUsuario comentarioUsuario = new ComentarioUsuario();
					listComentarios = comentarioUsuario.listarComentariosAvaliado(usuario);
					modelAndView.addObject("isMyUser" ,false);
					modelAndView.addObject("listComentarios", listComentarios);
					modelAndView.addObject("usuario" , usuario);
				}catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}else {
			modelAndView = new ModelAndView("login");
		}
		return modelAndView;
	}
	
	public List<Objetivo> listarObjetivos(Integer userId) throws Exception{
		Objetivo[] objetivos;
		List<Objetivo> listObjetivos = new ArrayList<Objetivo>();
		try {
			Objetivo objetivo = new Objetivo();
			objetivo.setId(userId);
			StringEntity params = new StringEntity(Util.convertObjectToJsonString(objetivo));
			String responseJSon = Util.criarChamadaHttpPost(Util.getProp(URL_LISTAGEM_OBJETIVO), params);
			objetivos = Util.convertJSONStringToObject(responseJSon, Objetivo[].class);
			listObjetivos = Arrays.asList(objetivos);
		}catch (Exception e) {
			throw new Exception();
		}
		return listObjetivos;
	}
	
	public Objetivo buscarObjetivo(String idObjetivo, String listObjetivosJson) {
		Objetivo objetivoSel = null;
		try {
			Objetivo[] objetivos;
			objetivos = Util.convertJSONStringToObject(listObjetivosJson, Objetivo[].class);
			List<Objetivo> listObjetivos = new ArrayList<Objetivo>();
			listObjetivos = Arrays.asList(objetivos);
			for(Objetivo objetivo : listObjetivos) {
				if(objetivo.getId().toString().equals(idObjetivo)) {
					objetivoSel = objetivo;
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return objetivoSel;
	}
	
	public List<ResultadoChave> listarResultados(Integer id) throws Exception{
		ResultadoChave[] resultadosChave;
		List<ResultadoChave> listResultadoChave = new ArrayList<ResultadoChave>();
		try {
			Objetivo objetivo = new Objetivo();
			objetivo.setId(id);
			objetivo.setListResultadoChave(null);
			List<Objetivo> objetivos = new ArrayList<Objetivo>();
			objetivos.add(objetivo);
			StringEntity params = new StringEntity(Util.convertObjectToJsonString(objetivos));
			String responseJSon = Util.criarChamadaHttpPost(Util.getProp(URL_LISTAGEM_RESULTADOS), params);
			resultadosChave = Util.convertJSONStringToObject(responseJSon, ResultadoChave[].class);
			listResultadoChave = Arrays.asList(resultadosChave);
		}catch (Exception e) {
			throw new Exception();
		}
		return listResultadoChave;
	}
	
	public List<Categoria> listarCategorias(){
		List<Categoria> listCategorias = new ArrayList<Categoria>();
		try {
			Categoria[] categorias;
			String responseJSon = Util.criarChamadaHttpPost(Util.getProp(URL_LISTA_CATEGORIAS));
			categorias = Util.convertJSONStringToObject(responseJSon, Categoria[].class);
			listCategorias = Arrays.asList(categorias);
		}catch (Exception e) {
			// TODO: handle exception
		}
		return listCategorias;
	}
	
}

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<jsp:include page="../../../imports.jsp" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Cadastro RateMe</title>
	<script type="text/javascript">
	</script>
</head>
<body>
	
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <div class="logo-nav">
		  <a class="navbar-brand" href="#" onclick="paginaInicial()">
		  	<img src="<c:url value="/resources/images/logo1.png" />">
		  </a>
	  </div>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	
	  <div class="collapse navbar-collapse" id="navbarSupportedContent">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item">
	        <a class="nav-link" href="#" onclick="home();">Início<span class="sr-only">(current)</span></a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="#" onclick="login();">Login</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="#" onclick="cadastro();">Cadastro</a>
	      </li>
	    </ul>
	  </div>
	</nav>
	
	<div class="jumbotron" id="pwd-container" style="width: 90%; background-color:#fff; margin: 5% auto;">
		<h1 class="mb-4">Bem vindo</h1>
		<c:if test="${erro}">
			<div style="margin-top: 20px" class="alert alert-warning" role="alert">
		  		${msgErro}
			</div>
		</c:if>
		<form action="cadastrarUsuario" method="post">
			<div class="row">
				<div class="form-group col-md-10" style="margin-top: 5px;">
					<span>Nome: </span>	
					<span>${cadastroRs.nome}</span>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-2" style="margin-top: 5px;">
					<span>Idade: </span>	
					<span>${cadastroRs.idade}</span>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-12" style="margin-top: 5px;">
					<span>Cargo: </span>
					<span>${cadastroRs.cargo.cargoNome}</span>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-12" style="margin-top: 5px;">
					<span>Descrição: </span>	
					<span>${cadastroRs.descUser}</span>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-12 "style="margin-top: 10px">
					<span>Email: </span>
					<span>${cadastroRs.email}</span>
				</div>
			</div>
			<center>
				<input style="margin-top: 25px; width: 30%" onclick="login();" value="Entrar" class="btn btn-lg btn-primary btn-block">
			</center>
		</form>
	</div>
</body>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<jsp:include page="../../../imports.jsp" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Cadastro RateMe</title>
	<script type="text/javascript">
	</script>
</head>
<body>
	
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <div class="logo-nav">
		  <a class="navbar-brand" href="#" onclick="home();">
		  	<img src="<c:url value="/resources/images/logo1.png" />">
		  </a>
	  </div>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	
	  <div class="collapse navbar-collapse" id="navbarSupportedContent">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item">
	        <a class="nav-link" href="#" onclick="home();">Início<span class="sr-only">(current)</span></a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="#" onclick="login();">Login</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="#" onclick="cadastro();">Cadastro</a>
	      </li>
	    </ul>
	  </div>
	</nav>
	
	<div class="jumbotron" id="pwd-container" style="width: 90%; background-color:#fff; margin: 5% auto;">
		<h1>Crie sua conta</h1>
		<c:if test="${erro}">
			<div style="margin-top: 20px" class="alert alert-warning" role="alert">
		  		${msgErro}
			</div>
		</c:if>
		<form action="cadastrarUsuario" method="post">
			<div class="row">
				<div class="form-group col-md-10" style="margin-top: 5px;">
					<span>Nome</span>	
					<input placeholder="Informe seu nome completo" name="nome" type="text" required="true" class="form-control input-lg">
				</div>
				<div class="form-group col-md-2" style="margin-top: 5px;">
					<span>Idade</span>	
					<input placeholder="Informe sua idade" name="idade"  type="text" required="true" class="form-control input-lg">
				</div>
			</div>
			<div>
				<span>Cargo</span>
				<select  name="cargo.cargoId" class="custom-select" id="inputGroupSelect04" aria-label="Example select with button addon" required="true">
					<option selected>Selecione o seu cargo...</option>
					<c:forEach items="${listCargos}" var="cargo">
				    	<option value="${cargo.cargoId}">${cargo.cargoNome}</option>
				    </c:forEach>
				</select>
			</div>
			<div style="margin-top: 5px">
				<span>Descrição</span>	
				<textarea placeholder="Faca uma breve descricao sobre seu cargo" name="descUser" class="form-control" aria-label="With textarea"></textarea>
			</div>
			<div class="row">
				<div class="form-group col-md-6 "style="margin-top: 10px">
					<span>Email</span>
					<input placeholder="Informe seu email" name="email" type="text" required="true" class="form-control input-lg">
				</div>
				<div class="form-group col-md-6" style="margin-top: 10px">
					<span>Confirmação de Email</span>
					<input placeholder="Confirme seu email" type="text" required="true" class="form-control input-lg">
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-6" style="margin-top: 5px">
					<span>Senha</span>
					<input placeholder="Informe sua senha" name="senha" type="password" required="true" class="form-control input-lg">
				</div>
				<div class="form-group col-md-6" style="margin-top: 5px">
					<span>Confirmação de senha</span>
					<input placeholder="Confirme sua senha" type="password" class="form-control input-lg">
				</div>
			</div>
			<center>
				<input style="margin-top: 25px; width: 40%" type="submit" value="Cadastrar" class="btn btn-lg btn-primary btn-block">
			</center>
		</form>
	</div>
</body>
</html>
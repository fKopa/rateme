<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="../../imports.jsp" />
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Tela Inicial</title>
<style>
</style>
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <div class="logo-nav">
		  <a class="navbar-brand" href="#" onclick="paginaInicial()">
		  	<img src="<c:url value="/resources/images/logo1.png" />">
		  </a>
	  </div>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	
	  <div class="collapse navbar-collapse" id="navbarSupportedContent">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item">
	        <a class="nav-link" href="#" onclick="paginaInicial()">Página inicial<span class="sr-only">(current)</span></a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="#" onclick="perfilUsuario()">Perfil</a>
	      </li>
	    </ul>
	  </div>
	</nav>
	
	<div class="container">
	    <div class="row profile">
			<div class="col-md-3">
				<div class="profile-sidebar">
					<!-- SIDEBAR USERPIC -->
					<center class="profile-userpic">
						<img src="<c:url value="/resources/images/user.png" />">
					</center>
					<div class="profile-usertitle">
						<div class="profile-usertitle-name">
							<span>${usuario.nome}</span>
						</div>
						<div class="profile-usertitle-job">
							<span>${usuario.cargo.cargoNome}</span>
						</div>
					</div>

					<div>
						<ul class="nav flex-column">
						  <li class="nav-item">
						    <a class="nav-link" href="#" onclick="pesquisaUsuarios()">Pesquisa de usuários</a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link active" href="#" onclick="listarComentarios()">Comentários</a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link" href="#" onclick="listarObjetivos()">Objetivos</a>
						  </li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-9">
	            <div class="profile-content">
	            	<h1>Pesquisa de usuários</h1>
	            	<form id="pesquisarusuario" action="pesquisarusuario" method="post">
		            	<div class="input-group mb-3">
						  <input name="pesquisa" type="text" class="form-control" placeholder="Pesquise por usuários" aria-label="Recipient's username" aria-describedby="button-addon2">
						  <div class="input-group-append">
						    <input type="submit" class="btn btn-outline-secondary" id="button-addon2" value="Pesquisar"/>
						  </div>
						</div>
					</form>
							
					<table class="table table-hover">
					  <thead class="thead-light">
					    <tr>
					      	<th scope="col">Nome</th>
					      	<th scope="col">Cargo</th>
					      	<th scope="col">Email</th>
					    </tr>
					  </thead>
					  <tbody>
					  	<c:forEach items="${listUsuarios}" var="user">
						    <tr onclick="perfilUsuarioSel(${user.id})">
						     	<td>
						     		<div>
										<img src="<c:url value="/resources/images/user1.png" />">
										<span>${user.nome}</span>
									</div>
						     	</td>
						      	<td style="vertical-align: middle !important;">${user.cargo.cargoNome}</td>
						      	<td style="vertical-align: middle !important;">${user.email}</td>
						    </tr>
					    </c:forEach>
					  </tbody>
					</table>
	            </div>
			</div>
		</div>
	</div>
	<form id="exibirperfilusuario" action="exibirperfilusuario" method="post">
		<input id="idUsuarioSel" type="hidden" name="idUsuarioSel">
		<input id="listUsuariosJson" type="hidden" name="listUsuariosJson" value='${listUsuariosJson}'>
	</form>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="../../../imports.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Cadastro de objetivos</title>
<style>
</style>
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <div class="logo-nav">
		  <a class="navbar-brand" href="#" onclick="paginaInicial()">
		  	<img src="<c:url value="/resources/images/logo1.png" />">
		  </a>
	  </div>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	
	  <div class="collapse navbar-collapse" id="navbarSupportedContent">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item">
	        <a class="nav-link" href="#" onclick="paginaInicial()">Página inicial<span class="sr-only">(current)</span></a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="#" onclick="perfilUsuario()">Perfil</a>
	      </li>
	    </ul>
	  </div>
	</nav>
	
	<div class="container">
	    <div class="row profile">
			<div class="col-md-3">
				<div class="profile-sidebar">
					<!-- SIDEBAR USERPIC -->
					<center class="profile-userpic">
						<img src="<c:url value="/resources/images/user.png" />">
					</center>
					<div class="profile-usertitle">
						<div class="profile-usertitle-name">
							<span>${usuario.nome}</span>
						</div>
						<div class="profile-usertitle-job">
							<span>${usuario.cargo.cargoNome}</span>
						</div>
					</div>
					<input onclick="cadastroObjetivo()" style="margin: 5%; width: 90%" value="Novo objetivo" class="btn btn-lg btn-secondary btn-block"/>
					<div>
						<ul class="nav flex-column">
						  <li class="nav-item">
						    <a class="nav-link" href="#" onclick="pesquisaUsuarios()">Pesquisa de usuários</a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link active" href="#" onclick="listarComentarios()">Comentários</a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link" href="#" onclick="listarObjetivos()">Objetivos</a>
						  </li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-9">
	            <div class="profile-content">
	            	<form action="cadastrarobjetivo" method="post">
		            	<h1>Objetivo</h1>
		            	<div class="row" style="margin-top: 10px">
			            	<div class="col-md-12">
								<span>Obejtivo: </span>
								<span>${objetivo.nome}</span>
							</div>
						</div>
		            	<div class="row" style="margin-top: 10px">
			            	<div class="col-md-12">
								<span>Descrição do seu objetivo: </span>
								<span>${objetivo.descricao}</span>
							</div>
						</div>
						<div class="row" style="margin-top: 10px">
			            	<div class="col-md-3">
								<span>Data limite: </span>
								<span>${objetivo.dataLimite}</span>
							</div>
						</div>
						<div class="row" style="margin-top: 10px">
			            	<div class="col-md-3">
								<span>Progresso: </span>
								<span>0%</span>
							</div>
						</div>
						<div class="col-md-12">
				            <div class="profile-content">
				            	<h4>Resultados chave</h4>
										
								<table class="table table-hover">
								  <thead class="thead-light">
								    <tr>
								      	<th scope="col">Resultado</th>
								      	<th scope="col">Data limite</th>
								      	<th scope="col">Progresso</th>
								    </tr>
								  </thead>
								  <tbody>
									<c:forEach items="${objetivo.listResultadoChave}" var="resultadoChave">
									  	<tr onclick="exibirObjetivo(${objetivo.id});" >
									      	<td style="vertical-align: middle !important;">${resultadoChave.nome}</td>
									      	<td style="vertical-align: middle !important;">${resultadoChave.dataLimite}</td>
									      	<td style="vertical-align: middle !important;">0%</td>
									    </tr>
								  	</c:forEach>
								  </tbody>
								</table>
				            </div>
						</div>
		            </form>
		            <form action="cadastroresultado" id="cadastroresultado" method="post">
						<center class="row" style="margin-top: 20px">
							<input id="objetivoJson" type="hidden" name="objetivoJson" value='${objetivoJson}'>
							<div class="col-md-3"></div>
			            	<div class="col-md-6">
			            		<input type="submit" value="Cadastrar Resultado" class="btn btn-lg btn-primary btn-block">
			            	</div>
			            </center>
		            </form>
					<center class="row" style="margin-top: 40px">
						<div class="col-md-3"></div>
		            	<div class="col-md-6">
		            		<input onclick="listarObjetivos()" value="Voltar" class="btn btn-lg btn-secondary btn-block">
		            	</div>
		            </center>
	            </div>
			</div>
		</div>
	</div>

</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="../../../imports.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Tela Inicial</title>
<style>
</style>
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <div class="logo-nav">
		  <a class="navbar-brand" href="#" onclick="paginaInicial()">
		  	<img src="<c:url value="/resources/images/logo1.png" />">
		  </a>
	  </div>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	
	  <div class="collapse navbar-collapse" id="navbarSupportedContent">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item">
	        <a class="nav-link" href="#" onclick="paginaInicial()">Página inicial<span class="sr-only">(current)</span></a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="#" onclick="perfilUsuario()">Perfil</a>
	      </li>
	    </ul>
	  </div>
	</nav>
	
	<div class="container">
	    <div class="row profile">
			<div class="col-md-3">
				<div class="profile-sidebar">
					<!-- SIDEBAR USERPIC -->
					<center class="profile-userpic">
						<img src="<c:url value="/resources/images/user.png" />">
					</center>
					<div class="profile-usertitle">
						<div class="profile-usertitle-name">
							<span>${usuario.nome}</span>
						</div>
						<div class="profile-usertitle-job">
							<span>${usuario.cargo.cargoNome}</span>
						</div>
					</div>
					<form id="cadastrocomentario" action="cadastrocomentario" method="post">
		            	<div class="input-group mb-3">
						  <input name="usuarioJson" type="hidden" value='${usuarioJson}'>
						  <div>
						    <input type="submit"  style="margin: 5%; width: 127%" value="Novo comentário" class="btn btn-lg btn-secondary btn-block"/>
						  </div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-9">
	            <div class="profile-content">
	            	<form action="cadastrarcomentario" id="cadastrarcomentario" method="post">
		            	<h1>Novo Comentário</h1>
		            	<div class="row">
			            	<div class="col-md-12">
								<span>Categoria do comentário</span>
								<select  name="idCategoria" class="custom-select" id="inputGroupSelect04" aria-label="Example select with button addon" required="true">
									<option selected>Selecione</option>
									<c:forEach items="${listCategorias}" var="categoria">
								    	<option value="${categoria.id}">${categoria.nomeCategoria}</option>
								    </c:forEach>
								</select>
							</div>
						</div>
		            	<div class="row">
			            	<div class="col-md-3">
								<span>Nota do comentário</span>
								<input name="nota" type="text" required="true" class="form-control input-lg"/>
							</div>
						</div>
						<div>
			            	<div style="margin-top: 5px">
								<span>Descrição</span>	
								<textarea name="descricao" style="height:200px;" placeholder="Faça uma descrição do seu comentário" class="form-control" aria-label="With textarea"></textarea>
							</div>
						</div>
						<input id="usuarioJson" type="hidden" name="usuarioJson" value='${usuarioJson}'>
						<center class="row" style="margin-top: 20px">
							<div class="col-md-3"></div>
			            	<div class="col-md-6">
			            		<input type="submit" value="Cadastrar" class="btn btn-lg btn-primary btn-block">
			            	</div>
			            </center>
		            </form>
		            <form id="comentarioperfil" action="comentarioperfil" method="post">
		            	<center class="row" style="margin-top: 20px">
		            		<div class="col-md-3"></div>
							<input id="usuarioJson" type="hidden" name="usuarioJson" value='${usuarioJson}'>
							<div class="col-md-6">
								<input type="submit" value="Descartar" class="btn btn-lg btn-secondary btn-block">
							</div>
						</center>
					</form>
	            </div>
			</div>
		</div>
	</div>

</body>
</html>
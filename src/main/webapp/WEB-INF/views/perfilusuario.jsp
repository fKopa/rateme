<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="../../imports.jsp" />
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Tela Inicial</title>
<style>
</style>
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <div class="logo-nav">
		  <a class="navbar-brand" href="#" onclick="paginaInicial()">
		  	<img src="<c:url value="/resources/images/logo1.png" />">
		  </a>
	  </div>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	
	  <div class="collapse navbar-collapse" id="navbarSupportedContent">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item">
	        <a class="nav-link" href="#" onclick="paginaInicial()">Página inicial<span class="sr-only">(current)</span></a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="#" onclick="perfilUsuario()">Perfil</a>
	      </li>
	    </ul>
	  </div>
	</nav>
	
	<div class="container">
	    <div class="row profile">
			<div class="col-md-3">
				<div class="profile-sidebar">
					<!-- SIDEBAR USERPIC -->
					<center class="profile-userpic">
						<img src="<c:url value="/resources/images/user.png" />">
					</center>
					<div class="profile-usertitle">
						<div class="profile-usertitle-name">
							<span>${usuario.nome}</span>
						</div>
						<div class="profile-usertitle-job">
							<span>${usuario.cargo.cargoNome}</span>
						</div>
					</div>
					
					<!-- Validar se é seu perfil ou não, se sim exibir lista se não exibir botão novo comentario -->
					<c:if test="${!isMyUser}">
						<form id="cadastrocomentario" action="cadastrocomentario" method="post">
			            	<div class="input-group mb-3">
							  <input name="usuarioJson" type="hidden" value='${usuarioJson}'>
							  <div>
							    <input type="submit"  style="margin: 5%; width: 127%" value="Novo comentário" class="btn btn-lg btn-secondary btn-block"/>
							  </div>
							</div>
						</form>
					</c:if>
					
					<c:if test="${isMyUser}">
						<input onclick="cadastroObjetivo()" style="margin: 5%; width: 90%" value="Novo objetivo" class="btn btn-lg btn-secondary btn-block">
					
						<div>
							<ul class="nav flex-column">
							  <li class="nav-item">
							    <a class="nav-link" href="#" onclick="pesquisaUsuarios()">Pesquisa de usuários</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link active" href="#" onclick="listarComentarios()">Comentários</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" href="#" onclick="listarObjetivos()">Objetivos</a>
							  </li>
							</ul>
						</div>
					</c:if>
				</div>
			</div>
			<div class="col-md-9">
	            <div class="profile-content">
	            	<h1>Perfil</h1>
	            	<h4>Rendimento</h4>
	            	<div id="container-line" style="height: 350px;"></div>         	
	            	<c:if test="${not empty listComentarios}">
		            	<h4>Destaques</h4>
		            	<table class="table table-striped">
						  <tbody>
						  	<c:forEach items="${listComentarios}" var="comentario">
							    <tr>
							      <th scope="row" style="width: 5%;">
							      	<figure id="container-nota">
									  <img src="<c:url value="/resources/images/circlulo-medio.png" />"> 
									  <figcaption>${comentario.nota}</figcaption>
									</figure>
							      </th>
							      <td><b>${comentario.nomeCategoria} - </b>${comentario.descricao}</td>
							    </tr>
						    </c:forEach>
						  </tbody>
						</table>
					</c:if>
					<c:if test="${!isMyUser}">
						<c:if test="${not empty listObjetivos}">
							<h4>Progresso dos Objetivos</h4>
							<c:forEach items="${listObjetivos}" var="objetivo">
			            		<span>${objetivo.nome}</span>
								<div class="progress mb-2">
								  <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
							</c:forEach>
						</c:if>
					</c:if>
	            </div>
			</div>
		</div>
	</div>
</body>
<script>
var jSonChartLine = ${chartLine};
Highcharts.chart('container-line', {
	title: {text: ''},
	
    subtitle: {enable: false},
    
    yAxis: {min: 0, max:10, title: {text: 'Nota de Progresso'}},
    
    xAxis: {min: 0, max:10},
    
    legend: {layout: 'vertical', align: 'right', verticalAlign: 'middle'},

	series: jSonChartLine,
	
	credits: {enabled: false},
    
    exporting: {enabled: false},
	
    responsive: {rules: [{condition: {maxWidth: 500}, chartOptions: {legend: {layout: 'horizontal', align: 'center', verticalAlign: 'bottom'}}}]}});
</script>
</html>
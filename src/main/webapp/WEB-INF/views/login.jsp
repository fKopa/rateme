<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<jsp:include page="../../imports.jsp" />
		<title>Login RateME</title>
		<script type="text/javascript">
		</script>
	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <div class="logo-nav">
			  <a class="navbar-brand" href="#" onclick="home();">
			  	<img src="<c:url value="/resources/images/logo1.png" />">
			  </a>
		  </div>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item">
		        <a class="nav-link" href="#" onclick="home();">Início<span class="sr-only">(current)</span></a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#" onclick="login();">Login</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#" onclick="cadastro();">Cadastro</a>
		      </li>
		    </ul>
		  </div>
		</nav>
		<div class="jumbotron" id="pwd-container" style="width: 400px; margin: 10% auto; background-color:#fff;">
			<c:if test="${erro}">
				<div class="alert alert-warning" role="alert">
			  		${msgErro}
				</div>
			</c:if>
			<div class="col-md-4"></div>
			<div class="col-md">
				<section class="login-form">
					<form name="login" action="logar" method="post">
						<input id="email" name="email" onblur="validacaoEmail(this)" type="text" placeholder="Email" required="true" class="form-control input-lg"/>
						<div class="invalid-feedback">
				          Informe um email valido
				        </div>
						<input name="senha" style="margin-top: 10px" type="password" class="form-control input-lg" id="Senha" placeholder="Senha" required="true" />
						<center style="margin: 5px">
							<a style="text-align: center;" href="#">Esqueci minha senha</a>
						</center>
						<button type="submit" value="logar" name="go" class="btn btn-lg btn-primary btn-block">Entrar</button>
						<input onclick="cadastro()" style="margin-top: 15px" value="Cadastrar" class="btn btn-lg btn-secondary btn-block">
					</form>
				</section>
			</div>
		</div>
	</body>
</html>
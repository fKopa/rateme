var path = "/"
function home(){
	window.location.href = "/rateme/";
}
function login(){
	window.location.href = "/rateme/login";	
};

function cadastro(){
	window.location.href = "/rateme/cadastro";
};

function paginaInicial(){
	window.location.href = "/rateme/paginainicial";
};

function perfilUsuario(){
	window.location.href = "/rateme/exibirperfilusuariologado";
};

function cadastroComentario(){
	window.location.href = "/rateme/cadastrocomentario";
};

function cadastroObjetivo(){
	window.location.href = "/rateme/cadastroobjetivo";
};

function pesquisaUsuarios(){
	window.location.href = "/rateme/pesquisausuarios";
};

function listarObjetivos(){
	window.location.href = "/rateme/listarobjetivos";
};

function listarComentarios(){
	window.location.href = "/rateme/comentariosusuario";
};

function exibirObjetivo(id){
	$("#idObjetivoSel").val(id);
	$("#exibirobjetivo").submit();
};

function perfilUsuarioSel(id){
	$("#idUsuarioSel").val(id);
	$("#exibirperfilusuario").submit();
};

function validacaoEmail(field) {
	usuario = field.value.substring(0, field.value.indexOf("@"));
	dominio = field.value.substring(field.value.indexOf("@")+ 1, field.value.length);
	 
	if ((usuario.length >=1) && (dominio.length >=3) && (usuario.search("@")==-1) && (dominio.search("@")==-1)
	    && (usuario.search(" ")==-1) && (dominio.search(" ")==-1) && (dominio.search(".")!=-1) && (dominio.indexOf(".") >=1) 
	    && (dominio.lastIndexOf(".") < dominio.length - 1)) {
		jQuery("#email").removeClass("is-invalid");
	}
	else{
		jQuery("#email").addClass("is-invalid");
	}
};
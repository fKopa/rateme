<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<jsp:include page="imports.jsp" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Página Inicial</title>
		<script type="text/javascript">
			function home(){
				window.location.href = "/rateme/";
			}
			function login(){
				window.location.href = "/rateme/login";	
			}
			
			function cadastro(){
				window.location.href = "/rateme/cadastro";
			}
		</script>
	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <div class="logo-nav">
			  <a class="navbar-brand" href="#" onclick="home();">
			  	<img src="<c:url value="/resources/images/logo1.png" />">
			  </a>
		  </div>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item">
		        <a class="nav-link" href="#" onclick="home();">Início<span class="sr-only">(current)</span></a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#" onclick="login();">Login</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#" onclick="cadastro();">Cadastro</a>
		      </li>
		    </ul>
		  </div>
		</nav>
		<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
		  <div class="carousel-inner">
		    <div class="carousel-item active">
		      <img class="d-block w-100" src="<c:url value="/resources/images/untitled.png" />" alt="First slide">
		    </div>
		    <div class="carousel-item">
		      <img class="d-block w-100" src="<c:url value="/resources/images/untitled2.png" />" alt="Second slide">
		    </div>
		  </div>
		  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
		    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
		    <span class="carousel-control-next-icon" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		</div>
	</body>
</html>